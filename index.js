class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    //Name
    get Name(){
        return this.name;
    }
    set Name (n) {
        this.name = n;
    }
    //Age
    get Age(){
        return this.age;
    }
    set Age(a){
        this.age = a;
    }
    //Salary
    get Salary(){
        return this.salary;
    }
    set Salary(s){
        this.salary = s;
    }
}
class Programmer extends Employee {
    constructor(name, age, salary){
        super(name, age, salary);
    }
    get Salary(){
        return `${this.salary * 3}$`;
    }
}

const worker1 = new Employee("John", 24, 700);
const worker2 = new Programmer("Adam", 32, 700);
const worker3 = new Programmer("Victor", 37, 700);

console.log(worker1);
console.log(worker2);
console.log(worker3);
